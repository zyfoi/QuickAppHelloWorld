(function(){
      
  var createAppHandler = function() {
    return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	var $app_script$ = __webpack_require__(38)
	
	$app_define$('@app-application/app', [], function($app_require$, $app_exports$, $app_module$){
	     $app_script$($app_module$, $app_exports$, $app_require$)
	     if ($app_exports$.__esModule && $app_exports$.default) {
	            $app_module$.exports = $app_exports$.default
	        }
	})
	
	$app_bootstrap$('@app-application/app',{ packagerVersion: '0.0.5'})


/***/ }),

/***/ 7:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var fetch = $app_require$('@app-module/system.fetch');
	var prompt = $app_require$('@app-module/system.prompt');
	
	var http = exports.http = {
	    GET: function GET(url, param, callback) {
	        fetch.fetch({
	            url: url,
	            method: 'GET',
	            data: param,
	            success: function success(res) {
	                console.log(res.data);
	                callback(res.data);
	            },
	            fail: function fail(res, code) {
	                console.log(url + ":\n" + res);
	                callback(res);
	            }
	        });
	    },
	
	    getMethod: function getMethod(url, param, callback) {
	        fetch.fetch({
	            url: url,
	            method: 'GET',
	            data: JSON.stringify(param),
	            contentType: 'application/json',
	            success: function success(res) {
	                console.log(JSON.stringify(res.data.sendData));
	                callback(res.data.sendData);
	            },
	            fail: function fail(res, code) {
	                console.log(url + ":\n" + res);
	                callback(res.data.sendDate);
	            }
	        });
	    },
	
	    postMethod: function postMethod(url, param, callback) {
	        console.log('[POST] ' + url);
	        console.log('[PARAM]  ' + JSON.stringify(param));
	        fetch.fetch({
	            url: url,
	            method: 'POST',
	            header: { 'Content-Type': 'application/json;charset=UTF-8' },
	            data: JSON.stringify(param),
	            success: function success(res) {
	                console.log('[CALLBACK] ' + res.data);
	                var obj = JSON.parse(res.data);
	                if (obj.e.code === 0) {
	                    callback(obj.data.sendData);
	                } else {
	                    prompt.showToast({
	                        message: obj.e.desc
	                    });
	                }
	            },
	            fail: function fail(res, code) {
	                console.log(res + " | " + "code= " + code);
	                callback(res);
	            }
	        });
	    }
	};

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var IP = "http://oaapi.yingfeng365.com/jsyf-oa";
	var URL = exports.URL = {
	    //登录
	    loginIn: IP + "/user/login.json",
	
	    //签到
	    signIn: IP + "/signIn/save.json",
	
	    //签到撤销
	    revertIn: IP + "/signIn/revertIn.json",
	
	    //签退撤销
	    revertOut: IP + "/signIn/revert.json",
	
	    //百度逆地址解析
	    getAddress: "http://api.map.baidu.com/geocoder/v2/?callback=&output=json&pois=0&ak=18eZsB5LvSoIf1HZOCUBDXKWHMbvhfpa&location=",
	
	    //查询签到
	    getSign: IP + "/signIn/getSignInByDay.json",
	
	    //上传文件
	    uploadFile: IP + "/attach/uploadFile.json",
	
	    //获取推荐
	    getRecommend: "https://www.apiopen.top/satinApi",
	
	    //获取美图
	    getPic: "https://www.apiopen.top/meituApi"
	
	};

/***/ }),

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = function(module, exports, $app_require$){'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _util = __webpack_require__(39);
	
	var _util2 = _interopRequireDefault(_util);
	
	var _system = $app_require$('@app-module/system.share');
	
	var _system2 = _interopRequireDefault(_system);
	
	var _common = __webpack_require__(7);
	
	var _UrlConfig = __webpack_require__(8);
	
	var _system3 = $app_require$('@app-module/system.router');
	
	var _system4 = _interopRequireDefault(_system3);
	
	var _system5 = $app_require$('@app-module/system.prompt');
	
	var _system6 = _interopRequireDefault(_system5);
	
	var _system7 = $app_require$('@app-module/system.storage');
	
	var _system8 = _interopRequireDefault(_system7);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	    showMenu: _util2.default.showMenu,
	    createShortcut: _util2.default.createShortcut,
	    userInfo: {},
	    pwd: '',
	    http: _common.http,
	    URL: _UrlConfig.URL,
	    share: _system2.default,
	    router: _system4.default,
	    prompt: _system6.default,
	    storage: _system8.default
	};
	(exports.default || module.exports).manifest = {"package":"com.guoqi.oatool","name":"OA助手","versionName":"1.0.5","versionCode":"5","minPlatformVersion":"1020","icon":"/Common/ic_launcher.png","features":[{"name":"system.prompt"},{"name":"system.router"},{"name":"system.shortcut"},{"name":"system.fetch"},{"name":"system.storage"},{"name":"system.image"},{"name":"system.geolocation"},{"name":"system.media"},{"name":"system.request"},{"name":"system.share"},{"name":"system.geolocation"},{"name":"system.network"}],"permissions":[{"origin":"*"}],"config":{"logLevel":"debug","designWidth":750},"router":{"entry":"Login","pages":{"Login":{"component":"index"},"Main":{"component":"index"},"Demo":{"component":"index"},"DemoDetail":{"component":"index"},"ShowImage":{"component":"index"},"About":{"component":"index"},"Video":{"component":"index"}}},"display":{"titleBarBackgroundColor":"#f2f2f2","titleBarTextColor":"#414141","menu":true,"pages":{"Login":{"titleBarText":"登录","menu":false},"Main":{"titleBarText":"办公","menu":false},"Demo":{"titleBarText":"","menu":false},"DemoDetail":{"titleBarText":"详情页"},"ShowImage":{"titleBarText":"查看图片","menu":false},"About":{"menu":false}}}};
	}

/***/ }),

/***/ 39:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * 显示菜单
	 */
	function showMenu() {
	  var prompt = $app_require$('@app-module/system.prompt');
	  var router = $app_require$('@app-module/system.router');
	  var appInfo = $app_require$('@app-module/system.app').getInfo();
	  prompt.showContextMenu({
	    itemList: ['保存桌面', '关于', '取消'],
	    success: function success(ret) {
	      switch (ret.index) {
	        case 0:
	          // 保存桌面
	          createShortcut();
	          break;
	        case 1:
	          // 关于
	          router.push({
	            uri: '/About',
	            params: {
	              name: appInfo.name,
	              icon: appInfo.icon
	            }
	          });
	          break;
	        case 2:
	          // 取消
	          break;
	        default:
	          prompt.showToast({
	            message: 'error'
	          });
	      }
	    }
	  });
	}
	
	/**
	 * 创建桌面图标
	 * 注意：使用加载器测试`创建桌面快捷方式`功能时，请先在`系统设置`中打开`应用加载器`的`桌面快捷方式`权限
	 */
	function createShortcut() {
	  var prompt = $app_require$('@app-module/system.prompt');
	  var shortcut = $app_require$('@app-module/system.shortcut');
	  shortcut.hasInstalled({
	    success: function success(ret) {
	      if (ret) {
	        prompt.showToast({
	          message: '已创建桌面图标'
	        });
	      } else {
	        shortcut.install({
	          success: function success() {
	            prompt.showToast({
	              message: '成功创建桌面图标'
	            });
	          },
	          fail: function fail(errmsg, errcode) {
	            prompt.showToast({
	              message: errcode + ': ' + errmsg
	            });
	          }
	        });
	      }
	    }
	  });
	}
	
	exports.default = {
	  showMenu: showMenu,
	  createShortcut: createShortcut
	};

/***/ })

/******/ });
  };
  if (typeof window === "undefined") {
    return createAppHandler();
  }
  else {
    window.createAppHandler = createAppHandler
    // H5注入manifest以获取features
    global.manifest = {"package":"com.guoqi.oatool","name":"OA助手","versionName":"1.0.5","versionCode":"5","minPlatformVersion":"1020","icon":"/Common/ic_launcher.png","features":[{"name":"system.prompt"},{"name":"system.router"},{"name":"system.shortcut"},{"name":"system.fetch"},{"name":"system.storage"},{"name":"system.image"},{"name":"system.geolocation"},{"name":"system.media"},{"name":"system.request"},{"name":"system.share"},{"name":"system.geolocation"},{"name":"system.network"}],"permissions":[{"origin":"*"}],"config":{"logLevel":"debug","designWidth":750},"router":{"entry":"Login","pages":{"Login":{"component":"index"},"Main":{"component":"index"},"Demo":{"component":"index"},"DemoDetail":{"component":"index"},"ShowImage":{"component":"index"},"About":{"component":"index"},"Video":{"component":"index"}}},"display":{"titleBarBackgroundColor":"#f2f2f2","titleBarTextColor":"#414141","menu":true,"pages":{"Login":{"titleBarText":"登录","menu":false},"Main":{"titleBarText":"办公","menu":false},"Demo":{"titleBarText":"","menu":false},"DemoDetail":{"titleBarText":"详情页"},"ShowImage":{"titleBarText":"查看图片","menu":false},"About":{"menu":false}}}};
  }
})();
//# sourceMappingURL=app.js.map